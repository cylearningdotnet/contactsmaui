﻿using Contact = Contacts.CoreBusiness.Contact;

namespace Contacts.UseCases.PluginInterfaces
{
    public interface IContactRepository

    {

        public Task<List<Contact>> GetContactsAsync(string filterText);
    }
}

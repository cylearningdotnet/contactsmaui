﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts.Maui.Models
{
    static class ContactRepository
    {
        private static List<Contact> _contacts = new List<Contact>()
        {
            new Contact() { ContactId=1, Name = "John Doe", Email = "john.doe@mail.com"},
            new Contact() { ContactId=2, Name = "Jane Doe", Email = "jane.doe@mail.com"},
            new Contact() { ContactId=3, Name = "Some One", Email = "some.one@mail.com"},
            new Contact() { ContactId=4, Name = "Another Guy", Email = "another.guy@mail.com"},
        };

        public static List<Contact> GetContacts() => _contacts;

        public static Contact GetContactById(int contactId)
        {
            var contact = _contacts.FirstOrDefault(x => contactId == x.ContactId);
            if (contact != null)
            {
                return new Contact()
                {
                    ContactId = contact.ContactId,
                    Name = contact.Name,
                    Email = contact.Email,
                    Phone = contact.Phone,
                    Address = contact.Address
                };
            }
            return null;
        }

        public static void UpdateContact(int contactId, Contact contact)
        {
            if (contactId != contact.ContactId) return;

            var contactToUpdate = _contacts.FirstOrDefault(x => contactId == x.ContactId);
            if (contactToUpdate != null)
            {
                contactToUpdate.Name = contact.Name;
                contactToUpdate.Email = contact.Email;
                contactToUpdate.Phone = contact.Phone;
                contactToUpdate.Address = contact.Address;
            }
        }

        public static void AddContact(Contact contact)
        {
            var maxId = _contacts.Max(x => x.ContactId);
            contact.ContactId = maxId + 1;
            _contacts.Add(contact);
        }

        public static void DeleteContact(int contactId)
        {
            var contact = _contacts.FirstOrDefault(x => x.ContactId == contactId);
            if (contactId != null)
            {
                _contacts.Remove(contact);
            }
        }

        public static List<Contact> SearchContacts(string query)
        {
            return _contacts.FindAll(x => ContactContainsText(x, query));
        }

        private static bool ContactContainsText(Contact contact, string query)
        {
            return ValueContainsText(contact.Name, query)
                || ValueContainsText(contact.Email, query)
                || ValueContainsText(contact.Phone, query)
                || ValueContainsText(contact.Address, query);
        }

        private static bool ValueContainsText(string value, string query)
        {
            return value != null && value.IndexOf(query, StringComparison.OrdinalIgnoreCase) >= 0;
        }
    }
}

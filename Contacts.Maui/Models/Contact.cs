﻿
namespace Contacts.Maui.Models
{
    public class Contact
	{
		public string Name { get; set; }
		public string Email { get; set; }
        public int ContactId { get; set; }
        public string Phone { get; set;}
        public string Address { get; set;}
    }
}
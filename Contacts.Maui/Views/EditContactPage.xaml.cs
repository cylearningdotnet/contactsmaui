using Contacts.Maui.Models;
using Contact = Contacts.Maui.Models.Contact;

namespace Contacts.Maui.Views;

[QueryProperty(nameof(ContactId), "Id")]
public partial class EditContactPage : ContentPage
{
	private Contact _contact;

	public EditContactPage()
	{
		InitializeComponent();
	}

	public string ContactId
	{
		set
		{
            _contact = ContactRepository.GetContactById(int.Parse(value));
			if (_contact != null)
			{
                contactControl.Name = _contact.Name;
                contactControl.Email= _contact.Email;
                contactControl.Phone = _contact.Phone;
                contactControl.Address = _contact.Address;
            }
		}
	}

    private void btnCancel_Clicked(object sender, EventArgs e)
    {
		Shell.Current.GoToAsync("..");
    }

    private void btnUpdate_Clicked(object sender, EventArgs e)
    {
		_contact.Name = contactControl.Name;
        _contact.Email = contactControl.Email;
        _contact.Phone = contactControl.Phone;
        _contact.Address = contactControl.Address;

		ContactRepository.UpdateContact(_contact.ContactId, _contact);
		Shell.Current.GoToAsync("..");
    }

    private void ContactControl_OnError(object sender, string e)
    {
        DisplayAlert("Error", e, "OK");
    }
}
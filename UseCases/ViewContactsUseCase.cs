﻿using Contact = Contacts.CoreBusiness.Contact;

namespace UseCases
{
    // All the code in this file is included in all platforms.
    public class ViewContactsUseCase
    {
        public async Task<List<Contact>> ExecuteAsync(string filterText)
        {
            return await this.contactRepository.GetContactsAsync(filterText);
        }
    }
}
